#!/usr/bin/env python
import PySimpleGUI as sg
import cv2
import numpy as np

import isbnlib
from isbnlib.dev._exceptions import NoDataForSelectorError
import json
import os
import time


frequency = 2500  # Set Frequency To 2500 Hertz
duration = 800  # Set Duration To 1000 ms == 1 second
rootpath = "./"
topcam = 0
devicecam = 1
SERVICE = 'goob'

#todo save images and increment page count only if save button is clicked
#todo let the user enter the page number in between scanning to go back
#show the current page number and status of saving in the window


#reference https://pypi.org/project/PySimpleGUI/

"""
Demo program that displays a webcam using OpenCV
"""


def main():

    #sg.change_look_and_feel('Material1')

    time.sleep(2.0)


    # define the window layout
    layout = [[sg.Text('book Scanner', size=(40, 1), justification='center', font='Helvetica 20')],
              [sg.Image(filename='', key='image')],
              [sg.Button('New Book', size=(10, 1), font='Helvetica 14'),
               sg.Button('Save Page', size=(10, 1), font='Any 14'),
               sg.Button('Quit', size=(10, 1), font='Helvetica 14'), ]]

    # create the window and show it without the plot
    window = sg.Window('Demo Application - OpenCV Integration',
                       layout, location=(800, 400))

    # ---===--- Event LOOP Read and display frames, operate the GUI --- #
    cap_device = cv2.VideoCapture(devicecam)
    cap_top = cv2.VideoCapture(topcam)
    saving_pages = False
    book_found = False
    book_folder = rootpath
    pagecount = 0

    book = {}

    book['isbn'] = 'not found'
    book['author'] = 'not found'
    book['title'] = 'not found'
    book['year'] = 'not found'
    book['publisher'] = 'not found'
    book['language'] = 'en'

    while True:
        event, values = window.read(timeout=20)
        if event == 'Quit' or event is None:
            # stop recording
            return

        elif event == 'New Book':
            #TODO automatically start the program in this mode
            # add more info into book object such as description thumbnail links

           book['isbn'] = 'not found'
           book['author'] = 'not found'
           book['title'] = 'not found'
           book['year'] = 'not found'
           book['publisher'] = 'not found'
           book['language'] = 'en'
           book_found = False

           layout = [[sg.Text('Your Title:'), sg.Text(size=(15, 1), key='-OUTPUT-')],
                      [sg.Input(key='-IN-')],
                      [sg.Button('Show'),sg.Button('Start Scanning'),sg.Button('Exit') ]]

            # Create the Window
           scan_window = sg.Window('Scanning ISBN', layout)
            # Event Loop to process "events" and get the "values" of the inputs
           while True:

               event, values = scan_window.read()
               barcodeData = values['-IN-']
               if barcodeData is not None:
                   if isbnlib.is_isbn10(barcodeData) or isbnlib.is_isbn13(barcodeData):  # only if valid isbn
                       try:
                           #todo replace google with amazon service
                           b_data = isbnlib.meta(barcodeData, SERVICE)
                           book['isbn'] = b_data['ISBN-13']
                           book['author'] = b_data['Authors'][0]
                           book['title'] = b_data['Title']
                           book['year'] = b_data['Year']
                           book['publisher'] = b_data['Publisher']
                           book['language'] = b_data['Language']


                           print(book)
                       except NoDataForSelectorError:
                           book['isbn'] = barcodeData
                           print('bar code info not found')
                           pass
                       book_found = True
                       book_folder = rootpath + barcodeData + '_' + book['title'] + '/'
                       if not os.path.exists(book_folder):
                           os.makedirs(book_folder)
                       with open(book_folder + barcodeData + '_' + book['title'] + '_bookinfo.txt', 'w') as outfile:
                           json.dump(book, outfile)



                       #todo check if book already exists in the csv file, if so ignore
                       bookdatafilename = rootpath + 'bookdata.csv'

                       with open(bookdatafilename,"a") as csvWrite:
                           csvWrite.write(
                               "{},{},{},{},{},{}\n".format(book['isbn'], book['author'], book['title'], book['year'],
                                                            book['publisher'], book['language']));
                       csvWrite.close()
               else:
                  book_found = False

               if event in (None, 'Exit'):
                   break
               if event == 'Show':
                   # TODO can we automatically do this, without having to click Show ?
                   scan_window['-OUTPUT-'].update(book['title'])

               if event == 'Start Scanning':
                   # TODO
                   if book_found == True:
                        saving_pages = True
                   else:
                       sg.Popup('Scan a new book ISBN to continue')  # Shows red error button
                       event = 'New Book'
                   break

               if event in (None, 'Exit'):  # if user closes window or clicks cancel
                   break

           scan_window.close()



        elif event == 'Save Page':
            if book_found == True:
                saving_pages = True
                pagecount = pagecount + 1
            else:
                sg.Popup('Scan a new book ISBN to continue')  # Shows red error button
                event = 'New Book'

        if saving_pages:
            # TODO check if a book id already exist , if not throw error
            if book_found == False:
                sg.Popup('Scan a new book ISBN to continue')  # Shows red error button
                event = 'New Book'
                saving_pages = False

            # take images from both cameras
            # save images
            # transcribe  image to text ?
            # convert text to audio ?

            # create subfolders for saving images
            #todo check if book folder already exists
            topcam_folder = book_folder + '/' + 'topcam/'
            if not os.path.exists(topcam_folder):
                os.makedirs(topcam_folder)

            devicecam_folder = book_folder + '/' + 'devicecam/'
            if not os.path.exists(devicecam_folder):
                os.makedirs(devicecam_folder)

            topcam_image = topcam_folder + str(pagecount) + '.bmp'
            device_cam_image = devicecam_folder + str(pagecount) + '.bmp'

            ret, frame_device = cap_device.read()
            ret, frame_top = cap_top.read()

            cv2.imwrite(device_cam_image, frame_device)
            cv2.imwrite(topcam_image, frame_top)

            frame_device_resized = cv2.resize(frame_device, (640, 480))
            frame_top_resized = cv2.resize(frame_top, (640, 480))

            frame = np.hstack((frame_device_resized, frame_top_resized))

            imgbytes = cv2.imencode('.png', frame)[1].tobytes()  # ditto
            window['image'].update(data=imgbytes)




main()