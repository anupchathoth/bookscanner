# USAGE
# python barcode_scanner.py

from imutils.video import VideoStream
from pyzbar import pyzbar
import argparse
import datetime
from datetime import datetime
import imutils
import time
import cv2
import winsound
import os
import isbnlib
from isbnlib.dev._exceptions import NoDataForSelectorError
import json

frequency = 2500  # Set Frequency To 2500 Hertz
duration = 800  # Set Duration To 1000 ms == 1 second
rootpath = "./"
topcam = 1
devicecam = 2
SERVICE = 'goob'

ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", type=str, default="barcodesData.csv",
                help="path to output CSV file ")
args = vars(ap.parse_args())



time.sleep(2.0)
csvWrite = open(args["output"], "a")
barcodefound = False

#scan books with barcode








while True:
    barcode = int(input())
    barcodeData = str(barcode)
    print('bar code found ' + barcodeData)

    if isbnlib.is_isbn10(barcodeData) or isbnlib.is_isbn13(barcodeData):  # only if valid isbn
        book = {}
        book['isbn'] = 'not found'
        book['author'] = 'not found'
        book['title'] = 'not found'
        book['year'] = 'not found'
        book['publisher'] = 'not found'
        book['language'] = 'en'
        #book['thumbnail'] = 'thumbnail'
        #book['pagecount'] = 'thumbnail'
        #book['description'] = 'thumbnail'

        try:
            b_data = isbnlib.meta(barcodeData, SERVICE)
            print(b_data)
            book['isbn'] = b_data['ISBN-13']
            book['author'] =  b_data['Authors'][0]
            book['title'] =  b_data['Title']
            book['year'] =  b_data['Year']
            book['publisher'] =  b_data['Publisher']
            book['language'] =  b_data['Language']
            #book['thumbnail'] = 'thumbnail'
            #book['pagecount'] = 'thumbnail'
            print(book)
            barcodefound = True
            currbook = book
            currbookisbn = barcodeData
            winsound.Beep(frequency, duration)

        except NoDataForSelectorError:
            book['isbn'] = barcodeData
            print('bar code info not found')
            pass

    else:
        print('bar code not valid')

    book_folder = rootpath + barcodeData + '_' + book['title'] + '/'
    if not os.path.exists(book_folder):
        os.makedirs(book_folder)
    with open(book_folder +  barcodeData + '_' + book['title'] + '_bookinfo.txt', 'w') as outfile:
        json.dump(book, outfile)

    csvWrite.write("{},{},{},{},{},{}\n". format(book['isbn'],book['author'], book['title'], book['year'],book['publisher'] ,book['language']) );
    csvWrite.flush()

